FROM node:18-alpine

WORKDIR /usr/src/app

COPY . .

RUN npm i --save asciidoctor @asciidoctor/reveal.js
RUN node convert-slide.js
RUN npm install http-server -g

EXPOSE 8080

CMD ["http-server"]
